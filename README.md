#### Create network
```
docker network create nginx
```

#### Startup:
Start containter:
```
docker-compose up -d
```

Stop containter:
```
docker-compose down
```

